from channels.generic.websocket import AsyncWebsocketConsumer
import json

class LobbyConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['lobby_name']
        self.room_group_name = 'chess_app_%s' % self.room_name

        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def receive(self, text_data):
        data_json = json.loads(text_data)
        
        if 'board' in data_json:
            board = data_json['board']    
            playerID = data_json['playerID']

            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'board_data',
                    'board': board,
                    'playerID': playerID
                }
            )

        elif 'status' in data_json:
            status = data_json['status']
            playerID = data_json['playerID']

            if status == 'joined':
                await self.channel_layer.group_send(
                    self.room_group_name,
                    {
                        'type': 'join_game',
                        'status': status,
                        'playerID': playerID,
                    }
                )

        elif 'player_count' in data_json:
            player_count = data_json['player_count']
            playerID = data_json['playerID']
            whiteID = data_json['whiteID']
            blackID = data_json['blackID']

            await self.channel_layer.group_send(
                    self.room_group_name,
                    {
                        'type': 'player_count',
                        'player_count': player_count,
                        'playerID': playerID,
                        'whiteID': whiteID,
                        'blackID': blackID,
                    }
                )

        elif 'current_turn' in data_json:
            current_turn = data_json['current_turn']
            
            await self.channel_layer.group_send(
                    self.room_group_name,
                    {
                        'type': 'turn_update',
                        'current_turn': current_turn,
                    }
            )

    async def board_data(self, event):
        board = event['board']
        playerID = event['playerID']

        await self.send(text_data=json.dumps({
            'board': board,
            'playerID': playerID,
        }))

    async def join_game(self, event):
        status = event['status']
        playerID = event['playerID']

        await self.send(text_data=json.dumps({
            'status': status,
            'playerID': playerID,
        }))

    async def player_count(self, event):
        player_count = event['player_count']
        playerID = event['playerID']
        whiteID = event['whiteID']
        blackID = event['blackID']

        await self.send(text_data=json.dumps({
            'player_count': player_count,
            'playerID': playerID,
            'whiteID': whiteID,
            'blackID': blackID,
        }))

    async def turn_update(self, event):
        current_turn = event['current_turn']
        await self.send(text_data=json.dumps({
            'current_turn': current_turn,
        }))
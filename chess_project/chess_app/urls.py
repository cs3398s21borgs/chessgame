from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='chessgame-home'),
    path('chess_app/', views.home, name='chessgame-home'),
    path('chess_app/<str:lobby_name>/', views.lobby, name='lobby'),
    path('contact/', views.contact, name='contact-us'),
    path('about/', views.about),
    path('games/', views.games),
    path('index_home/', views.index_home),
]

class Board {
    constructor() {
        this.board = [['br','bn','bb','bq','bk','bb','bn','br'],
                      ['bp','bp','bp','bp','bp','bp','bp','bp'],
                      ['ec','ec','ec','ec','ec','ec','ec','ec'],
                      ['ec','ec','ec','ec','ec','ec','ec','ec'],
                      ['ec','ec','ec','ec','ec','ec','ec','ec'],
                      ['ec','ec','ec','ec','ec','ec','ec','ec'],
                      ['wp','wp','wp','wp','wp','wp','wp','wp'],
                      ['wr','wn','wb','wq','wk','wb','wn','wr']];

        this.prev_en_passant;

        this.white_king = player_type == "White" ? {'i': 7, 'j': 4} : {'i': 0, 'j': 4};
        this.prev_white;
        this.black_king = player_type == "White" ? {'i': 0, 'j': 4} : {'i': 7, 'j': 4};
        this.prev_black;

        this.left_black_rook = true, this.right_black_rook = true;
        this.left_white_rook = true, this.right_white_rook = true;
        this.w_k_first = true, this.b_k_first = true;
    }

    // Returns board array to be used by front-end
    getState() {
        return this.board;
    }

    /* Query possible moves from chess board
    * curr_x: current i index of the piece.
    * curr_y: current j index of the piece.
    * move_logic: function that returns an array of objects that each represent
    *               a potential move. The object must have keys 'i' and 'j'!
    * returns: an array of objects that each represent a move that the player
    *               may make on the board.
    */
    queryMove(curr_x, curr_y, move_logic, first_move) {
        let piece = this.board[curr_x][curr_y];
        let potential_moves = move_logic(curr_x, curr_y, first_move);
        let possible_moves = [];

        if(player_type != current_turn)
            return possible_moves;

        for(let idx = 0; idx < potential_moves.length; idx++) {
            let moveObject = potential_moves[idx];
            let i = moveObject['i'];
            let j = moveObject['j'];

            let outOfArray = (i < 0 || j < 0) || (i > 7 || j > 7);
            if(outOfArray) continue;

            let pieceCollision = false;
            if(piece.charAt(0) == this.board[i][j].charAt(0)) {
                pieceCollision = true;
            } 

            if(!pieceCollision)
                possible_moves.push(moveObject);       
        }        
        return possible_moves;
    }

    /* Moves a piece on the board, send updated board to other player
     * curr_x: current i index of the piece.
     * curr_y: current j index of the piece.
     * moveObject: object representing selected move, chosen from possible moves
     * 
     * Will set board_changed to true!
     * Websocket will send new board information to other player!
     */
    move(curr_x, curr_y, moveObject) {
        let piece = this.board[curr_x][curr_y];
        let passant_l = false, passant_r = false;
        let new_i = moveObject['i'];
        let new_j = moveObject['j'];
        let temp = this.board[new_i][new_j];
        let temp_pawn;
        this.board[new_i][new_j] = this.board[curr_x][curr_y];
        this.board[curr_x][curr_y] = 'ec';

        //en passant move
        if(piece.charAt(1) == 'p')
        {

            if ((curr_y - new_j) == 1 ) {

                passant_r = true;
                temp_pawn = this.board[new_i+1][new_j]
                this.board[new_i+1][new_j] = 'ec';
            }
            if ((curr_y - new_j) == -1 ) {
                passant_l = true;
                temp_pawn = this.board[new_i+1][new_j]
                this.board[new_i+1][new_j] = 'ec';
            }
            
        }
        
        //promotion to queen
        if (piece.charAt(1) == 'p' && new_i == 0) {
            this.board[new_i][new_j] = piece.charAt(0) + 'q';
        }

        //castling maneuver check
        if(piece.charAt(1) == 'k' && new_j - curr_y == 2) {
            this.board[7][new_j - 1] = this.board[7][7];
            this.board[7][7] = 'ec';
            if (player_type == 'White') {
                this.w_k_first = false;
                this.right_white_rook = false;
            }
            else {
                this.b_k_first = false;
                this.right_black_rook = false;
            }
        } else if (piece.charAt(1) == 'k' && new_j - curr_y == -2) {
            this.board[7][new_j + 1] = this.board[7][0];
            this.board[7][0] = 'ec';
            if (player_type == 'White') {
                this.w_k_first = false;
                this. left_white_rook = false;
            }
            else {
                this.b_k_first = false;
                this.left_black_rook = false;
            }
        }
        
        // checking if the move causes check.

        let king_coords = player_type == 'White' ? this.white_king : this.black_king;
        let king_i = king_coords['i'];
        let king_j = king_coords['j'];
        if(this.king_not_move_into_check(king_i, king_j)) {
            this.rook_moved(curr_x, curr_y, piece);
            if(piece == 'bk') {
                this.b_k_first = false;
            } else if(piece == 'wk') {
                this.w_k_first = false;
            }

            board_changed = true;

            if(player_type === 'White') current_turn = 'Black';
            else if(player_type === 'Black') current_turn = 'White';

            

            chatSocket.send(JSON.stringify({'board': this.board, 'playerID': playerID}));
            chatSocket.send(JSON.stringify({'current_turn': current_turn}));
            }
        else {
            //bad move, puts king in check
            this.board[curr_x][curr_y] = this.board[new_i][new_j];
            this.board[new_i][new_j] = temp;
            if(passant_r) {
                this.board[new_i+1][new_j] = temp_pawn;
            } else if (passant_l) {
                this.board[new_i-1][new_j] = temp_pawn;
            }
            return false;

        }
    }
    querySpaceEmpty(i, j) {
        if (i > 7 || j > 7 || i < 0 || j < 0) {
            return false;
        }
        return this.board[i][j] == 'ec';

    }

    querySpace(i, j) {
        if (i > 7 || j > 7 || i < 0 || j < 0) {
            return 'ec';
        }
        return this.board[i][j];
    }

    /* Sets board to a new board recieved by websocket 
     * websocket calls function to update board!
     * board_changed is set so update() method in game.js will know
     * to draw a new board.
     */
    setBoard(newBoard) {
        this.prev_en_passant = this.board[1]
        this.board = newBoard;
        board_changed = true;
    }
    setKingLocation(i, j, isBlack) {
        if (isBlack) {
            this.prev_black = this.black_king;
            this.black_king = {'i': i, 'j': j};
        }
        else {
            this.prev_white = this.white_king;
            this.white_king = {'i': i, 'j': j};
        }
    }

    king_not_move_into_check(i, j) {
        if (i > 7 || j > 7 || i < 0 || j < 0) {
            return false;
        }
        let color = player_type == "White" ? 'b' : 'w';
        if(this.attack_moves(i, j, check_pawn, color + 'p')) {
            return false;
        } else if (this.attack_moves(i, j, check_king, color + 'k')){
            return false;
        } else if (this.attack_moves(i, j, move_knight, color + 'n')) {
            return false;
        } else if (this.attack_moves(i, j, move_bishop, color + 'b')) {
            return false;
        } else if (this.attack_moves(i, j, move_rook, color + 'r')){
            return false;
        } else if (this.attack_moves(i, j, move_queen, color + 'q')){
            return false;
        }
        return true;
    }
    rook_moved(curr_x, curr_y, piece) {
        if (piece.charAt(1) == 'r') {
            if ((curr_x == 7 && curr_y == 7 && player_type == 'White' && this.right_white_rook)) {
                this.right_white_rook = false;
            } else if(curr_x == 7 && curr_y == 7 && player_type == 'Black' && this.right_black_rook) {
                this.right_black_rook = false;
            } else if (curr_x == 7 && curr_y == 0 && player_type == 'Black' && this.left_black_rook) {
                this.left_black_rook = false;
            } else if (curr_x == 7 && curr_y == 0 && player_type == 'White' && this.left_white_rook) {
                this.left_white_rook = false;
            }
        }
    }

    attack_moves(i, j, move_logic, piece) {
        let attacks = move_logic(i,j);

        for(let idx = 0; idx < attacks.length; idx++) {
            let moveObject = attacks[idx];
            let new_i = moveObject['i'];
            let new_j = moveObject['j'];
            if (this.querySpace(new_i,new_j) == piece) {
                return true;
            }
        }
        return false;
    }
    check_castling_left(i, j) {
        if (player_type == 'White' && this.w_k_first && this.left_white_rook && this.querySpaceEmpty(7, 1)) {
            if(this.king_not_move_into_check(i, j) && this.king_not_move_into_check(i, j-1) && this.king_not_move_into_check(i, j-2) && this.querySpaceEmpty(i,j-1))
            {
                return true;
            }
        }
        else if (player_type == 'Black' && this.b_k_first && this.left_black_rook && this.querySpaceEmpty(7, 1)) {
            if(this.king_not_move_into_check(i, j) && this.king_not_move_into_check(i, j-1) && this.king_not_move_into_check(i, j-2) && this.querySpaceEmpty(i,j-1))
            {
                return true;
            }
        }
    }
    check_castling_right(i , j) {
        if (player_type == 'White' && this.w_k_first && this.right_white_rook && this.querySpaceEmpty(7, 6)) {
            if(this.king_not_move_into_check(i, j) && this.king_not_move_into_check(i, j+1) && this.king_not_move_into_check(i, j+2) && this.querySpaceEmpty(i,j+1))
            {
                return true;
            }
        }
        else if (player_type == 'Black' && this.b_k_first && this.right_black_rook && this.querySpaceEmpty(7, 6)) {
            if(this.king_not_move_into_check(i, j) && this.king_not_move_into_check(i, j+1) && this.king_not_move_into_check(i, j+2) && this.querySpaceEmpty(i,j+1))
            {
                return true;
            }
        }
    }
    check_en_passant(i, j, isLeft) {
        let color = player_type == 'White' ? 'b' : 'w';
        let mod = isLeft ? 1 : -1
        if (this.prev_en_passant == null) {
            return false
        }
        if(i == 3) {
            if (this.querySpace(i, j+mod) == (color + 'p') && this.prev_en_passant[j+mod] == color + 'p')
            {
                return true;
            }
        }
    }
}
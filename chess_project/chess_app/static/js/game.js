var config = {
    type: Phaser.AUTO,
    width: game_dimension,
    height: game_dimension,
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var game = new Phaser.Game(config);

function create() {
    draw(this);
    disable_pieces();
}

function update() {
    if(board_changed) {
        draw(this);
        disable_pieces();
        board_changed = false;
        document.getElementById("turn").innerHTML = "Current Turn: " + current_turn;
        document.getElementById("player").innerHTML = "Playing as: " + player_type;
    }
}

function draw(object) {
    var graphics = object.add.graphics();
    white_pieces = []
    black_pieces = []
    // Draw inital chess board
    let board_array = board.getState();
    
    for(let i = 0; i < 8; i++) {
        for(let j = 0; j < 8; j++) {
            let cell_color = ((i+j) % 2 != 0) ? player_type == "White" ? color_brown : color_tan : player_type == "White" ? color_tan : color_brown;
            graphics.fillStyle(cell_color);
            graphics.fillRect(i * cell_size, j * cell_size, cell_size, cell_size);
            let sprite_conf = {scene:object,x:((i * cell_size) + off_set), y:(j * cell_size) + off_set, img:board_array[j][i]};
            let sprite;

            // LORD FORGIVE ME FOR THE HORROR I'M ABOUT TO WRITE - LOGAN 
            if(board_array[j][i] == 'ec') {
                continue;
            }
            switch(board_array[j][i]) {
                case "bk":
                    sprite = new Piece(sprite_conf, i, j, move_king);
                    board.setKingLocation(j, i, true);
                    break;
                case "wk":
                    sprite = new Piece(sprite_conf, i, j, move_king);
                    board.setKingLocation(j, i, false);
                    break;
                case "bn":
                case "wn":
                    sprite = new Piece(sprite_conf, i, j, move_knight);
                    break;
                case "br":
                case "wr":
                    sprite = new Piece(sprite_conf, i, j, move_rook);
                    break;
                case "bb":
                case "wb":
                    sprite = new Piece(sprite_conf, i, j, move_bishop);
                    break;
                case "bp":
                case "wp":
                    sprite = new Piece(sprite_conf, i, j, move_pawn);
                    break;
                case "bq":
                case "wq":
                    sprite = new Piece(sprite_conf, i, j, move_queen);
                    break;
            }
            if(sprite.color == 'black') {
                black_pieces.push(sprite);
            } else {
                white_pieces.push(sprite);
            }
            
        }
    }
}

function disable_pieces() {
    
    disable_black();
    disable_white();
    if(player_type == "White" && current_turn == "White") {
        enable_white();
    }
    if(player_type == "Black" && current_turn == "Black") {
        enable_black();
    }
}

function disable_black() {
    for(let i = 0; i < black_pieces.length; i++) {
        black_pieces[i].disableInteractive();
    }
}
function enable_black() {
    for(let i = 0; i < black_pieces.length; i++) {
        black_pieces[i].setInteractive();
    }
}
function disable_white() {
    for(let i = 0; i < white_pieces.length; i++) {
        white_pieces[i].disableInteractive();
    }
}
function enable_white() {
    for(let i = 0; i < white_pieces.length; i++) {
        white_pieces[i].setInteractive();
    }
}
//for constants that touch all JS files
const game_dimension = 800;
const color_tan = 0xffce9e, color_brown = 0xd18b47;
const cell_size = game_dimension / 8;
const off_set = cell_size / 2;
var board_changed = false;
var board = new Board();
var playerID;
var player_count = 1;
var player_type = 'Spectator'; // White, Black, Spectator
var current_turn = 'White';

// Used to keep track of who is white and who is black
var whiteID = null;
var blackID = null;

black_pieces = [];
white_pieces = [];

function flip_board(board) {
    for(let i = 0, j = 7; i < 4; i++, j--) {
        let temp = board[i];
        board[i] = board[j];
        board[j] = temp;
    }
}

// Move logic functions for chess pieces
// These define the possibles moves a piece may take given the pieces array indices
function base_move(i, j) {
    return [];
}
function check_pawn(i, j) {
    move_logic = []
    if (!board.querySpaceEmpty(i+1, j+1)) {
        move_logic.push({'i':i+1, 'j':j+1});
    }
    if (!board.querySpaceEmpty(i+1, j-1)) {
        move_logic.push({'i':i+1, 'j':j-1});
    }
    return move_logic;
}

function check_king(i, j, firstMove) {
    let move_logic = [{'i':i-1, 'j':j-1}, {'i':i-1, 'j':j}, {'i':i-1, 'j':j+1}, {'i':i, 'j':j-1}, 
    {'i':i, 'j':j+1}, {'i':i+1, 'j':j-1}, {'i':i+1, 'j': j}, {'i':i+1, 'j':j+1}];

    return move_logic;
}

function move_pawn(i, j) {
    let move_logic = []

    if (board.querySpaceEmpty(i-1,j)) {
        move_logic.push({'i':i-1, 'j':j});
    }
    if (!board.querySpaceEmpty(i-1, j-1)) {
        move_logic.push({'i':i-1, 'j':j-1});
    }
    if (!board.querySpaceEmpty(i-1, j+1)) {
        move_logic.push({'i':i-1, 'j':j+1});
    }
    if(i == 6 && board.querySpaceEmpty(i-1 ,j)) {
        move_logic.push({'i':i-2, 'j':j})
    }
    if(board.check_en_passant(i,j, true)) {
        move_logic.push({'i':i-1, 'j':j+1})
    }
    if(board.check_en_passant(i,j, false)) {
        move_logic.push({'i':i-1, 'j':j-1})
    }
    return move_logic; 
};

function move_king(i, j) {
    let move_logic = [];
    
    if(board.king_not_move_into_check(i-1,j)) {
        move_logic.push({'i':i-1, 'j':j}) 

    }
    if(board.king_not_move_into_check(i,j-1)) {
        move_logic.push({'i':i, 'j':j-1}) 

    }
    if(board.king_not_move_into_check(i-1,j-1)) {
        move_logic.push({'i':i-1, 'j':j-1}) 

    }
    if(board.king_not_move_into_check(i+1,j+1)) {
        move_logic.push({'i':i+1, 'j':j+1}) 

    }
    if(board.king_not_move_into_check(i+1,j)) {
        move_logic.push({'i':i+1, 'j':j}) 

    }
    if(board.king_not_move_into_check(i,j+1)) {
        move_logic.push({'i':i, 'j':j+1}) 

    }
    if(board.king_not_move_into_check(i-1,j+1)) {
        move_logic.push({'i':i-1, 'j':j+1}) 

    }
    if(board.king_not_move_into_check(i+1,j-1)) {
        move_logic.push({'i':i+1, 'j':j-1}) 
    }
    //castling check
    if(board.check_castling_left(i, j)) {
        move_logic.push({'i':i, 'j':j-2})
    }
    if(board.check_castling_right(i,j)) {
        move_logic.push({'i':i, 'j':j+2})
    }
    return move_logic;
}

function move_queen(i, j) {
    let move_logic = [];
    let not_collide = [true, true, true, true, true, true, true, true];

    for(let k = 1; k < 8; k++) {
        let dir = 0;
        if(not_collide[dir] && board.querySpaceEmpty(i+k,j)) {
            move_logic.push({'i':i+k, 'j':j});
        } else if (not_collide[dir]) {
            move_logic.push({'i':i+k, 'j':j});
            not_collide[dir] = false;
        }
        dir++;
        if(not_collide[dir] && board.querySpaceEmpty(i-k, j)) {
            move_logic.push({'i':i-k, 'j':j});
        } else if (not_collide[dir]) {
            move_logic.push({'i':i-k, 'j':j});
            not_collide[dir] = false;
        }
        dir++;
        if(not_collide[dir] && board.querySpaceEmpty(i, j+k)) {
            move_logic.push({'i':i, 'j':j+k});
        } else if (not_collide[dir]) {
            move_logic.push({'i':i, 'j':j+k});
            not_collide[dir] = false;
        }
        dir++;
        if(not_collide[dir] && board.querySpaceEmpty(i, j-k)) {
            move_logic.push({'i':i, 'j':j-k});
        } else if (not_collide[dir]) {
            move_logic.push({'i':i, 'j':j-k});
            not_collide[dir] = false;
        }
        dir++;
        if(not_collide[dir] && board.querySpaceEmpty(i+k, j+k)) {
            move_logic.push({'i':i+k, 'j':j+k});
        } else if (not_collide[dir]) {
            move_logic.push({'i':i+k, 'j':j+k});
            not_collide[dir] = false;
        }
        dir++;
        if(not_collide[dir] && board.querySpaceEmpty(i+k, j-k)) {
            move_logic.push({'i':i+k, 'j':j-k});
        } else if (not_collide[dir]) {
            move_logic.push({'i':i+k, 'j':j-k});
            not_collide[dir] = false;
        }
        dir++;
        if(not_collide[dir] && board.querySpaceEmpty(i-k, j-k)) {
            move_logic.push({'i':i-k, 'j':j-k});
        } else if (not_collide[dir]) {
            move_logic.push({'i':i-k, 'j':j-k});
            not_collide[dir] = false;
        }
        dir++;
        if(not_collide[dir] && board.querySpaceEmpty(i-k, j+k)) {
            move_logic.push({'i':i-k, 'j':j+k});
        } else if (not_collide[dir]) {
            move_logic.push({'i':i-k, 'j':j+k});
            not_collide[dir] = false;
        }
        dir++;
    }

    return move_logic;
}

function move_rook(i, j) {
    let move_logic = [];
    let not_collide = [true, true, true, true];

    for(let k = 1; k < 8; k++) {
        let dir = 0;
        if(not_collide[dir] && board.querySpaceEmpty(i+k,j)) {
            move_logic.push({'i':i+k, 'j':j});
        } else if (not_collide[dir]) {
            move_logic.push({'i':i+k, 'j':j});
            not_collide[dir] = false;
        }
        dir++;
        if(not_collide[dir] && board.querySpaceEmpty(i-k, j)) {
            move_logic.push({'i':i-k, 'j':j});
        } else if (not_collide[dir]) {
            move_logic.push({'i':i-k, 'j':j});
            not_collide[dir] = false;
        }
        dir++;
        if(not_collide[dir] && board.querySpaceEmpty(i, j+k)) {
            move_logic.push({'i':i, 'j':j+k});
        } else if (not_collide[dir]) {
            move_logic.push({'i':i, 'j':j+k});
            not_collide[dir] = false;
        }
        dir++;
        if(not_collide[dir] && board.querySpaceEmpty(i, j-k)) {
            move_logic.push({'i':i, 'j':j-k});
        } else if (not_collide[dir]) {
            move_logic.push({'i':i, 'j':j-k});
            not_collide[dir] = false;
        }
    }

    return move_logic;
}

function move_bishop(i, j) {
    let move_logic = [];
    let not_collide = [true, true, true, true];

    for(let k = 1; k < 8; k++) {
        let dir = 0;
        if(not_collide[dir] && board.querySpaceEmpty(i+k, j+k)) {
            move_logic.push({'i':i+k, 'j':j+k});
        } else if (not_collide[dir]) {
            move_logic.push({'i':i+k, 'j':j+k});
            not_collide[dir] = false;
        }
        dir++;
        if(not_collide[dir] && board.querySpaceEmpty(i+k, j-k)) {
            move_logic.push({'i':i+k, 'j':j-k});
        } else if (not_collide[dir]) {
            move_logic.push({'i':i+k, 'j':j-k});
            not_collide[dir] = false;
        }
        dir++;
        if(not_collide[dir] && board.querySpaceEmpty(i-k, j-k)) {
            move_logic.push({'i':i-k, 'j':j-k});
        } else if (not_collide[dir]) {
            move_logic.push({'i':i-k, 'j':j-k});
            not_collide[dir] = false;
        }
        dir++;
        if(not_collide[dir] && board.querySpaceEmpty(i-k, j+k)) {
            move_logic.push({'i':i-k, 'j':j+k});
        } else if (not_collide[dir]) {
            move_logic.push({'i':i-k, 'j':j+k});
            not_collide[dir] = false;
        }
        dir++;
    }

    return move_logic;
}

function move_knight(i, j) {
    let move_logic = [{'i':i-1, 'j':j-2}, {'i':i-2, 'j':j-1}, {'i':i-1, 'j':j+2}, {'i':i-2, 'j':j+1},
                      {'i':i+1, 'j':j-2}, {'i':i+2, 'j':j-1}, {'i':i+1, 'j':j+2}, {'i':i+2, 'j':j+1}];

    return move_logic;
}
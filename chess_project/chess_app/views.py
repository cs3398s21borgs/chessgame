from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'chess_app/index.html', {'title':'index'})

def home(request):
    return render(request, 'chess_app/home.html', {'title':'home'})

def lobby(request, lobby_name):
    return render(request, 'chess_app/lobby.html', {'lobby_name': lobby_name})

def contact(request):
    return render(request, 'chess_app/contact.html', {'title':'contact-us'})

def about(request):
    return render(request, 'chess_app/about.html', {'title':'about-us'})

def games(request):
    return render(request, 'chess_app/games.html', {'title':'games'})

def index_home(request):
    return render(request, 'chess_app/index_home.html', {'title':'index-Home'})

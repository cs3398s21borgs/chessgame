# Chessgame

## Status
Project is: _in progress_,

team's accomplishment:

* Logan Hodgins: Added other players and implemented turns (https://bitbucket.org/cs3398s21borgs/chessgame/commits/88718e6b42a469600030cd6353538eb920097e8d, https://bitbucket.org/cs3398s21borgs/chessgame/commits/3d91b70f269b977d9e8017cbdbca2ff61e2eb2c8)

* Daniel Carpenter: Made it possible to deploy app to web (https://bitbucket.org/cs3398s21borgs/chessgame/commits/6cb71e119ce3f69fc000d93b23107fcd025aed18)

* Francisco Martell:  created gamehub homepages (https://bitbucket.org/cs3398s21borgs/chessgame/commits/3994739f190e5f701b2cb0b2636f97ba343775a3)


team's next step:

* Logan Hodgins: Fix issues with Chrome.

* Daniel Carpenter: Fix issues with Chrome.

* Francisco Martell: Fix gamehub links.


## How to run django web server
* For Windows Subsystem for Linux & Ubuntu: ```python3 manage.py runserver```

> Chessgame is an online browser-based chess game of the future, intended for chess enthusiasts and beginners.
> Our web app will impact the world by connecting different people through a friendly game of chess.
> It is made by Logan Hodgins, Daniel Carpenter, Francisco Martell.

## Table of contents
* [General info](#general-info)
* [Screenshots](#screenshots)
* [Technologies](#technologies)
* [Setup](#setup)
* [Features](#features)
* [Status](#status)
* [Inspiration](#inspiration)
* [Contact](#contact)

## General info
Add more general information about project. What the purpose of the project is? Motivation?

## Screenshots
![<img src="https://cdn.pixabay.com/photo/2016/07/12/11/39/checkmate-1511866_960_720.jpg">](https://cdn.pixabay.com/photo/2016/07/12/11/39/checkmate-1511866_960_720.jpg)

## Technologies
* Gameplay is made possible by Phaser for javascript
* Webpages are styled with SASS!
* Backend server is made with Django and Django Channels

## Setup
Describe how to install / setup your local environement / add link to demo version.

## Code Examples
Show examples of usage:
`put-your-code-here`

## Features
List of features ready and TODOs for future development
To-do list:

* Easy Invitation: Our game will allow you to easily invite your friends to a game of chess. This feature will finally allow you to easily play with your friends stuck in quarantine.

* Consistent UI: Chessgame looks great on both the browser and on mobile. This means you will be able to take your chess game with you wherever you go.

* Challenging AI: Chessgame provides an AI that will challenge your chess skills. Don't cry if you have no one to play with :(, with AI, friends aren't needed.

## Inspiration
This project was inspired by Chess.com

## Contact
Created by: Logan Hodgins, Daniel Carpenter, Francisco Martell.
